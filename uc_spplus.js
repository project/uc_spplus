/**
 * @name uc_spplus module js file
 */

self.name="retourspplus";

function Ouvrir_Spplus()
{
      // Largeur et hauteur pr�conis�es de la fen�tre SPPLUS
  var PopupSpplus_largeur  = 740;
  var PopupSpplus_hauteur  = 535;

    // Position haut et gauche de la fen�tre SPPLUS pour affichage centr� dans l'�cran
  var PopupSpplus_top  =((screen.height-PopupSpplus_hauteur)/2);
  var PopupSpplus_left  =((screen.width-PopupSpplus_largeur)/2);

  // Ouverture du popup SPLUS avec barre �tat uniquement et focus sur la fen�tre
  var win = window.open('', "SPPLUS","status=yes,top="+PopupSpplus_top+",left="+PopupSpplus_left+",width="+PopupSpplus_largeur+",height="+PopupSpplus_hauteur);
  win.focus();
}