<?php

/**
 * @file
 * SPplus menu items.
 *
 */

function uc_spplus_complete($cart_id = 0) {
  global $user;

  if (!isset($_GET['refsfp'])) {
    drupal_set_message(t('You are not allowed to see this page, please, return to homepage.'));
    drupal_goto('cart');
  }
  // debug
  if (variable_get('uc_spplus_debug', FALSE) == TRUE) {
    foreach ($_GET as $k => $v) {
      drupal_set_message($k .': '. $v);
    }
  }

  // get order info
  $order_id = explode('_', $_GET['reference']);
  $order = uc_order_load($order_id[0]);
  $status = uc_order_status_data($order->order_status, 'state');

  if ($order === FALSE || $status != 'in_checkout') {
    // try to re-validate or error ?
    if ($status == 'payment_received') {
      drupal_set_message(t('Your payment has already been made.'));
      watchdog('uc_spplus', 'ERROR : user @userid try to re-validate.', array('@userid' => $user->uid), WATCHDOG_WARNING);
      drupal_goto('user');
    }
    else {
      drupal_set_message(t('An error has occurred during payment. Please try again or contact us.'), 'error');
      watchdog('uc_spplus', 'ERROR : no order found', NULL, WATCHDOG_WARNING);
      drupal_goto('cart');
    }
  }

  // check hmac
  $hmac = _uc_spplus_hmac($_SERVER['QUERY_STRING'], $_GET);

  if ($hmac['ok']) {
    // get state
    $state = _uc_spplus_etat($hmac['etat']);
    // add every spplus notification
    uc_order_comment_save($order->order_id, $user->uid, $state['infos'], 'admin');
    // debug
    if (variable_get('uc_spplus_debug', FALSE) == TRUE) {
      drupal_set_message($state['infos']);
    }
    // Check state
    if ($state['response'] == 'ACCEPTED') {
      // Empty that cart...
      uc_cart_empty($cart_id);
      // process
      uc_payment_enter($order->order_id, 'spplus', $_GET['montant'], $user->uid, NULL, $state['infos']);
      uc_order_comment_save($order->order_id, $user->uid, t('Order paid (@modalite) via website with SPplus', array('@modalite' => $_GET['modalite'])), 'order', $order->order_status, FALSE);
      $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

      $page = variable_get('uc_cart_checkout_complete_page', '');
      if (!empty($page)) {
        drupal_goto($page);
      }
      return $output;
    }
    elseif ($state['response'] == 'CANCELED') {
      unset($_SESSION['cart_order']);
      uc_order_comment_save($order->order_id, $user->uid, t('This order has been explicitly canceled by the user.'), 'order');
      if (uc_order_update_status($order->order_id, uc_order_state_default('canceled'))) {
        drupal_set_message(t('Your order has been canceled.'), 'status');
      }
      else {
        drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
        watchdog('uc_spplus', "The order @order couldn't be marked as 'Canceled'.", array('@order' => $order->order_id), WATCHDOG_WARNING, NULL);
      }
      drupal_goto('cart');
    }
    else {
      drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
      watchdog('uc_spplus', 'No state response on order @order.', array('@order' => $order->order_id), WATCHDOG_WARNING, NULL);
    }
  }
  else {
    drupal_set_message(t('An error has occured during payment, please contact us.'), 'error');
    watchdog('uc_spplus', 'HMAC ERROR', NULL, WATCHDOG_WARNING);
    drupal_goto('cart');
  }
  return TRUE;
}

function uc_spplus_spcheck() {

  // Nous répondons SYSTEMATIQUEMENT au serveur SPPLUS
  // We MUST respond to SPplus server request before processing (?)
  echo("spcheckok");

  // IP Spplus server check
  $remote_addr = ip_address();

  if ($remote_addr && _uc_spplus_ip_check($remote_addr)) {
    $hmac = _uc_spplus_hmac($_SERVER['QUERY_STRING'], $_GET);

    if ($hmac['ok']) {
      // get state
      $state = _uc_spplus_etat($hmac['etat']);
      // get order id from reference
      $order_id = explode('_', $_GET['reference']);
      // get admin comment on every notifications
      uc_order_comment_save($order_id[0], 0, t('SPplus server notification: ') . $state['infos'], 'admin');
      watchdog('uc_spplus', 'SPplus server notification');
    }
  }
  else {
    watchdog('uc_spplus', 'SPplus server ip error : @ip', array('@ip' => $remote_addr), WATCHDOG_WARNING);
  }
  exit;
}

function _uc_spplus_hmac($param, $get) {

  $output['ok'] = FALSE;
  // init variables
  $montant = $reference = $refsfp = $etat = 0;
  $email = $info = $etat_comm = $etat_commb = '';
  // Bad news, SPplus use GET ! :(
  if (isset($get['montant'])) {
    $montant = $get['montant'];
  }
  if (isset($get['reference'])) {
    $reference = $get['reference'];
  }
  if (isset($get['refsfp'])) {
    $refsfp = $get['refsfp'];
  }
  if (isset($get['etat'])) {
    $etat = $get['etat'];
  }
  if (isset($get['email'])) {
    $email = $get['email'];
  }
  $output['etat'] = $etat;
  $output['montant'] = $montant;
  $output['refsfp'] = $refsfp;
  // no drupal url ?q=cart/spplus/complete&
  $chaine_param = preg_replace("/q=[^&]+&/", "", $_SERVER['QUERY_STRING']);

  // official SPplus script
  $pos = strpos($chaine_param, "&hmac=");

  if ($pos == FALSE || !is_integer($pos)) {
    watchdog('uc_spplus', 'ERROR : NO HMAC', NULL, WATCHDOG_WARNING);
  }
  else {
    $chaine_calcul = substr($chaine_param, 0, $pos);
    $chaine_calcul .= substr($chaine_param, $pos+strlen($_GET["hmac"])+6, strlen($chaine_param));
    $chaine_param = '';
    $tok = strtok($chaine_calcul, "=&");
    while ($tok) {
      if ($_REQUEST[$tok] != "") {
        $tok = strtok("&=");
        $chaine_param .= $tok;
      }
      $tok = strtok("&=");
      $tok = urldecode($tok);
    }
    if (variable_get('uc_spplus_cle', '') == '') {
      $clent = '58 6d fc 9c 34 91 9b 86 3f fd 64 63 c9 13 4a 26 ba 29 74 1e c7 e9 80 79';
    }
    else {
      $clent = variable_get('uc_spplus_cle', '');
    }

    $hmac_calcule = nthmac($clent, $chaine_param);
    // compare Hmac
    if (strcmp($hmac_calcule, $get["hmac"]) == 0) {
      $output['ok'] = TRUE;
    }
    else {
      watchdog('uc_spplus', 'ERROR : HMAC COMPARE FAILED', NULL, WATCHDOG_WARNING);
    }
  }
  return $output;
}

function _uc_spplus_etat($etat) {
  $output['response'] = '';
  switch ($etat) {
    case 0:
      $output['infos'] = t('state=0 : Nothing done.');
      break;
    case 1:
      $output['infos'] = t('state=1 : Payment accepted - empty cart - order confirmed.');
      $output['response'] = 'ACCEPTED';
      break;
    case 2:
      $output['infos'] = t('state=2 : Payment canceled - empty cart - order canceled.');
      $output['response'] = 'CANCELED';
      break;
    case 4:
      $output['infos'] = t('state=4 : Deadline of payment accepted and awaiting delivery - order confirmed.');
      break;
    case 5:
      $output['infos'] = t('state=5 : Deadline for payment refused - empty cart - order canceled.');
      break;
    case 6:
      $output['infos'] = t('state=6 : Payment accepted by check - empty cart - Pending order.');
      break;
    case 8:
      $output['infos'] = t('state=8 : Check cashed - order confirmed.');
      break;
    case 10:
      $output['infos'] = t('state=10 : Payment completed.');
      break;
    case 11:
      $output['infos'] = t('state=11 : Deadline for payment canceled by the trader - empty cart - order canceled.');
      break;
    case 12:
      $output['infos'] = t('state=12 : Customer canceled - empty cart - order canceled.');
      break;
    case 15:
      $output['infos'] = t('state=15 : Refund registered - processing customer account.');
      break;
    case 16:
      $output['infos'] = t('state=16 : Refund canceled - processing customer account.');
      break;
    case 17:
      $output['infos'] = t('state=17 : Refund accepted - processing customer account.');
      break;
    case 20:
      $output['infos'] = t('state=20 : Deadline of payment with an outstanding - processing customer account.');
      break;
    case 21:
      $output['infos'] = t('state=21 : Deadline of payment with an outstanding and awaiting validation SPplus services - processing customer account.');
      break;
    case 30:
      $output['infos'] = t('state=30 : Deadline of payment stored.');
      break;
    case 99:
      $output['infos'] = t('state=99 : Payment of production test - empty cart - order canceled.');
      (variable_get('uc_spplus_debug', FALSE) == TRUE) ? $output['response'] = 'ACCEPTED' : '';
      break;
    default:
      $output['infos'] = t('Unknown state : @state.', array('@state' => $etat));
  }
  return $output;
}

